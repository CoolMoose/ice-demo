import Ice.Current;
import RandomWalk.Node;
import RandomWalk.NodeBody;
import RandomWalk.Path;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Daniel on 4/13/2017.
 */
public class CollectorI extends RandomWalk._CollectorDisp{

  GraphServer.GraphPanel helper;
  int maxLength = 0;

  public CollectorI(GraphServer.GraphPanel frame){
    super();
    this.helper = frame;
  }


  // Clients will use this method to return results
  @Override
  public void sendPath(Path start, int length, Current __current) {
    if(length >= maxLength){
      helper.setPath(start);
      helper.drawpath(start, 0, helper);
    }
  }


  // Operation to get the graph from the server
  @Override
  public Node getGraph(Current __current) {

    // Notice since NodeBody extends Node, this is okay
    NodeBody starter =(NodeBody) helper.findStart();

    return starter;
  }
}
