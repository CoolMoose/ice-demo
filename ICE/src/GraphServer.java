import RandomWalk.Node;
import RandomWalk.NodeBody;
import RandomWalk.Path;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Daniel on 4/13/2017.
 */
public class GraphServer {
  final public int width = 1900;
  final public int height = width / 16 * 9;


  int status = 0;
  Ice.Communicator ic = null;

  public GraphServer(String[] args){
    try{
      // Start Up Ice
      ic = Ice.Util.initialize(args);

      // Create and end point
      Ice.ObjectAdapter adapter = ic.createObjectAdapterWithEndpoints("GraphCollectorAdapter", "default -p 10000");

      // Create the Ice Object implementation
      Ice.Object collectorObj = new CollectorI(startFrame());

      // Tell end point there's a new interface
      adapter.add(collectorObj, ic.stringToIdentity("GraphCollector"));

      // Start listening
      adapter.activate();

      // Run indefinitely
      ic.waitForShutdown();

      // Possible Error Catching
    } catch (Ice.LocalException e) {
      e.printStackTrace();
      status = 1;
    } catch (Exception e) {
      System.err.println(e.getMessage());
      status = 1;
    }

    if (ic != null) {
      try {
        ic.destroy();
      } catch (Exception e) {
        System.err.println(e.getMessage());
        status = 1;
      }
    }
    System.exit(status);
  }

  private GraphPanel startFrame(){
    final String title = "Test Window";


    //Creating the frame.
    JFrame frame = new JFrame(title);

    frame.setSize(width, height);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setVisible(true);

    //Creating the canvas.
    GraphPanel helper = new GraphPanel();


    helper.setSize(width, height);
    helper.setBackground(Color.WHITE);

    //Putting it all together.
    frame.add(helper);



    helper.setVisible(true);
    helper.setFocusable(false);

    return helper;
  }

  public static void main(String[] args)
  {
    new GraphServer(args);
  }

  public class GraphPanel extends JPanel {

    ArrayList<Node> nodes = new ArrayList<>();
    Map<Node, ArrayList<Node>> nodeToNeighbors = new HashMap<>();
    Path currentPath;

    final int NUMBEROFTRIES = 100000;

    final int NUMOFVERT = 10000;
    final int NUMOFEDEGS = 5000;
    final int VERTWIDTH = 6;

    GraphPanel(){
      generateGraph();
      repaint();
    }

    public void paintComponent(Graphics g)
    {
      g.setColor(Color.white);
      g.fillRect(0,0, width,height);
      drawVert(g);
      drawpath(currentPath, 0, this);
    }

    Node findStart(){
      int randomX = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);
      return nodes.get(randomX);
    }

    void generateGraph(){
      nodes = new ArrayList<>();

      for(int i = 0; i < NUMOFVERT; ++i){

        int randomX = ThreadLocalRandom.current().nextInt(VERTWIDTH, width - VERTWIDTH + 1);
        int randomY = ThreadLocalRandom.current().nextInt(VERTWIDTH, height - VERTWIDTH + 1);
        NodeBody n = new NodeBody();
        n.x = randomX;
        n.y = randomY;
        nodes.add(n);
      }

      for(int i = 0; i < NUMOFEDEGS; ++i){
        int randomX = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);
        int randomY = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);

        Node nodeX = nodes.get(randomX);
        Node nodeY = nodes.get(randomY);

        if(nodeToNeighbors.containsKey(nodeX)){
          nodeToNeighbors.get(nodeX).add(nodeY);
        }else{
          ArrayList<Node> newList = new ArrayList<>();
          newList.add(nodeY);
          nodeToNeighbors.put(nodeX, newList);
        }

        if(nodeToNeighbors.containsKey(nodeY)){
          nodeToNeighbors.get(nodeY).add(nodeX);
        }else{
          ArrayList<Node> newList = new ArrayList<>();
          newList.add(nodeX);
          nodeToNeighbors.put(nodeY, newList);
        }
      }

      for(Node n: nodes){
        java.util.List<Node> neighbors = nodeToNeighbors.getOrDefault(n, new ArrayList<>());
        ((NodeBody)n).neighbors = new Node[neighbors.size()];
        for(int i = 0; i < neighbors.size(); i++){
          ((NodeBody)n).neighbors[i] = neighbors.get(i);
        }
      }
    }

    void setPath(Path path){
      this.currentPath = path;
    }

    void drawpath(Path path, int index, JPanel frame) {
      Graphics2D g = (Graphics2D) frame.getGraphics();

      if (path != null) {
        if (index == 0) {
          NodeBody start = (NodeBody) path.me;
          g.setColor(Color.GREEN);
          g.setStroke(new BasicStroke(5));
          g.drawOval(start.x - VERTWIDTH, start.y - VERTWIDTH, VERTWIDTH * 2, VERTWIDTH * 2);
        }

        if (path.next != null) {
          g.setColor(Color.blue);
          g.setStroke(new BasicStroke(2));
          NodeBody me = (NodeBody) path.me;
          Path picked = path.next;
          NodeBody pickedNode = (NodeBody) picked.me;
          g.drawLine(me.x, me.y, pickedNode.x, pickedNode.y);

          drawpath(picked, index + 1, frame);
        }
      }
    }

    void drawVert(Graphics g){
      for(Node n : nodes){
        Graphics2D g2 = (Graphics2D)g;
        NodeBody node = (NodeBody)n;
        for(Node ed : ((NodeBody)n).neighbors){
          NodeBody e = (NodeBody)ed;
          Color prev = g.getColor();
          g2.setColor(Color.black);
          Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{3}, 0);
          g2.setStroke(dashed);
          g2.drawLine(node.x, node.y, e.x, e.y);
          g2.setColor(prev);
        }

        Color prev = g.getColor();
        g.setColor(Color.orange);
        g.fillOval(node.x-(VERTWIDTH/2),node.y-(VERTWIDTH/2),VERTWIDTH,VERTWIDTH);
        g.setColor(prev);
      }
    }
  }

}
