


import org.graphstream.graph.Graph;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Server {
  public Server(String[] args){
    int status = 0;
    Ice.Communicator ic = null;
    try {
      ic = Ice.Util.initialize(args);
      Ice.ObjectAdapter adapter = ic.createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 10000");
      Ice.Object object = new PrinterI();
      adapter.add(object, ic.stringToIdentity("SimplePrinter"));
      adapter.activate();
      ic.waitForShutdown();
    } catch (Ice.LocalException e) {
      e.printStackTrace();
      status = 1;
    } catch (Exception e) {
      System.err.println(e.getMessage());
      status = 1;
    }
    if (ic != null) {
      // Clean up
      //
      try {
        ic.destroy();
      } catch (Exception e) {
        System.err.println(e.getMessage());
        status = 1;
      }
    }
    System.exit(status);
  }



  public static void main(String[] args)
  {
    new Server(args);
  }
}