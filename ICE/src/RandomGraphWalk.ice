module RandomWalk{

    // Create class for extension and use in sub classes
    class Node{};

    // Create a new type: sequence of nodes
    sequence<Node> Neighbors;

    // Extend Node to hold data and edge references
    class NodeBody extends Node{
        int x;
        int y;
        bool visited;
        Neighbors neighbors;
    };

    // Return type from workers - 'linked list'
    class Path{
        Node me;
        Path next;
    };

    // Tell interface what methods are available
    interface Collector{
        void sendPath(Path start, int length);
        Node getGraph();
    };

};