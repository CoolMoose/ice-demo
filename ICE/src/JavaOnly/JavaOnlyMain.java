package JavaOnly;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class JavaOnlyMain {

  final int width = 1900;
  final int height = width / 16 * 9;

  final int NUMBEROFTRIES = 100000;

  final int NUMOFVERT = 10000;
  final int NUMOFEDEGS = 5000;
  final int VERTWIDTH = 6;


  public static void main(String[] args)
  {
    new JavaOnlyMain();
  }

  public JavaOnlyMain(){

    final String title = "Test Window";


    //Creating the frame.
    JFrame frame = new JFrame(title);

    frame.setSize(width, height);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setVisible(true);

    //Creating the canvas.
    GraphHelper helper = new GraphHelper();

    helper.setSize(width, height);
    helper.setBackground(Color.WHITE);

    //Putting it all together.
    frame.add(helper);



    helper.setVisible(true);
    helper.setFocusable(false);
  }

  public class GraphHelper extends JPanel implements MouseListener {
    ArrayList<Node> nodes = new ArrayList<>();

    GraphHelper(){
      this.addMouseListener(this);
      generateGraph();
    }

    void generateGraph(){

      nodes = new ArrayList<>();

      for(int i = 0; i < NUMOFVERT; ++i){
        int randomX = ThreadLocalRandom.current().nextInt(VERTWIDTH, width - VERTWIDTH + 1);
        int randomY = ThreadLocalRandom.current().nextInt(VERTWIDTH, height - VERTWIDTH + 1);
        Node n = new Node(randomX,randomY);
        nodes.add(n);
      }

      for(int i = 0; i < NUMOFEDEGS; ++i){
        int randomX = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);
        int randomY = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);
        nodes.get(randomX).addEdge(nodes.get(randomY));
        nodes.get(randomY).addEdge(nodes.get(randomX));
      }
    }

    void clearPath(){
      for(Node n : nodes){
        n.visited = false;
      }
    }



    Node findStart(){
      int randomX = ThreadLocalRandom.current().nextInt(0, NUMOFVERT);
      return nodes.get(randomX);
    }

    public void paintComponent(Graphics g)
    {
      g.setColor(Color.white);
      g.fillRect(0,0, width,height);
      drawVert(g);
    }

    void drawVert(Graphics g){
      for(Node n : nodes){
        n.draw(g);
      }
    }

    Path findPath(Path path){
      Collections.shuffle(path.me.neighbors);
      for(Node n : path.me.neighbors){
        if(!n.visited){
          n.visited = true;
          Path append = new Path();
          append.me = n;
          path.next = append;
          return findPath(path.next);
        }
      }
      return path;
    }

    int pathLength(Path path){
      int hops = 0;
      while(path != null){
        hops ++;
        path = path.next;
      }
      return hops;
    }

    void drawpath(Path path, int index){
      Graphics2D g = (Graphics2D) getGraphics();

      if(path != null){
        if(index == 0){
          Node start = path.me;
          g.setColor(Color.GREEN);
          g.setStroke(new BasicStroke(5));
          g.drawOval(start.x - VERTWIDTH, start.y - VERTWIDTH,VERTWIDTH * 2, VERTWIDTH * 2);
        }

        if(path.next != null){
          g.setColor(Color.blue);
          g.setStroke(new BasicStroke(2));
          Node me = path.me;
          Path picked = path.next;
          g.drawLine(me.x, me.y, picked.me.x, picked.me.y);

          drawpath(picked, index + 1);
        }
      }
    }


    @Override
    public void mouseClicked(MouseEvent e) {
      repaint();
      Path longestPath = new Path();

      int maxHop = 0;
      for(int i = 0; i < NUMBEROFTRIES; i++){
        Path startPath = new Path();
        startPath.me = findStart();
        clearPath();
        startPath.me.visited = true;
        findPath(startPath);
        int testLength = pathLength(startPath);


        if(testLength > maxHop){
          maxHop = testLength;
          longestPath = startPath;
        }
      }

      System.out.println("Longest Path: " + maxHop);

      Path finalPath = longestPath;
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          clearPath();
          drawpath(finalPath, 0);
        }
      });

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


    class Path{
      Node me;
      Path next = null;
    }

    class Node{
      int x,y;
      boolean visited = false;

      ArrayList<Node> neighbors = new ArrayList<>();

      Node(int x ,int y)
      {
        this.x = x;
        this.y = y;
      }

      void addEdge(Node n){
        neighbors.add(n);
      }

      void draw(Graphics g){
        Graphics2D g2 = (Graphics2D)g;
        for(Node e : neighbors){
          Color prev = g.getColor();
          g2.setColor(Color.black);
          Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{3}, 0);
          g2.setStroke(dashed);
          g2.drawLine(x, y, e.x, e.y);
          g2.setColor(prev);
        }

        Color prev = g.getColor();
        g.setColor(Color.orange);
        g.fillOval(x-(VERTWIDTH/2),y-(VERTWIDTH/2),VERTWIDTH,VERTWIDTH);
        g.setColor(prev);
      }
    }
  }

}
