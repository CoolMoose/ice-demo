import sys, traceback, Ice
import RandomWalk

SAMPLES = 10000

def findPath(path):
    for node in path.me.neighbors:
        if not node.visited:
            node.visited = True
            newPath = RandomWalk.Path()
            newPath.me = node
            path.next = newPath
            return findPath(path.next)
    return path

def pathLength(path):
    hops = 0
    while(path != None):
        hops += 1
        path = path.next
    return hops


status = 0
ic = None
try:
    ic = Ice.initialize(sys.argv)
    base = ic.stringToProxy("GraphCollector:default -p 10000")
    collector = RandomWalk.CollectorPrx.checkedCast(base)
    if not collector:
        raise RuntimeError("Invalid proxy")



    longestPath = None
    maxPath = 0
    for x in range(0, SAMPLES):
        node = collector.getGraph()
        startPath = RandomWalk.Path()
        startPath.me = node
        startPath.me.visited = True
        findPath(startPath)

        testLength = pathLength(startPath)

        if testLength > maxPath:
            maxPath = testLength
            longestPath = startPath


    collector.sendPath(longestPath, maxPath)
    print("Longests: ", maxPath)





except:
    traceback.print_exc()
    status = 1

if ic:
    # Clean up
    try:
        ic.destroy()
    except:
        traceback.print_exc()
        status = 1

sys.exit(status)