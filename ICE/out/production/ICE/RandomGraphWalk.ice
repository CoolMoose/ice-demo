module RandomWalk{

    class Node{};

    sequence<Node> Neighbors;

    class NodeBody extends Node{
        int x;
        int y;
        bool visited;
        Neighbors neighbors;
    };

    class Path{
        Node me;
        Path next;
    };

    interface Collector{
        void sendPath(Path start, int length);
        Node getGraph();
    };

};